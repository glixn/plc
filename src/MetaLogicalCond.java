public abstract class MetaLogicalCond extends MetaCond {
    MetaCond left;
    MetaCond right;
    Label bridgeLabel;

    public MetaLogicalCond(MetaCond left, MetaCond right) {
        this.left = left;
        this.right = right;
        bridgeLabel = Storage.newLabel();
    }

    @Override
    public String condStatement() {
        return left.condStatement()
                + bridgeLabel.labelStm()
                + right.condStatement();
    }

    @Override
    public abstract void setTrueLabel(Label trueLabel);

    @Override
    public abstract void setFalseLabel(Label falseLabel);
}
