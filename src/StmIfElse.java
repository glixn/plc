public class StmIfElse extends MetaStm {
    private MetaCond condition;
    private Label exitLabel;
    private MetaStm statement1;
    private MetaStm statement2;

    public StmIfElse(MetaCond condition, MetaStm statement1, MetaStm statement2) {
        this.condition = condition;
        this.statement1 = statement1;
        this.statement2 = statement2;
        this.exitLabel = Storage.newLabel();
        condition.setTrueLabel(Storage.newLabel());
        condition.setFalseLabel(Storage.newLabel());
    }

    @Override
    public String getCode() {
        return condition.condStatement()
                + condition.getTrueLabel().labelStm()
                + statement1.getCode()
                + exitLabel.gotoStm()
                + condition.getFalseLabel().labelStm()
                + statement2.getCode()
                + exitLabel.labelStm();
    }
}
