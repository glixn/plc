public class StmPrint extends MetaStm {
    private MetaExpr expr;

    public StmPrint(MetaExpr expr) {
        this.expr = expr;
    }

    @Override
    public String getCode() {
        return expr.preStatement() + "print " + expr.value() + ";\n";
    }
}
