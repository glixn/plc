public class CondOr extends MetaLogicalCond {
    public CondOr(MetaCond left, MetaCond right) {
        super(left,right);
        left.setFalseLabel(bridgeLabel);
    }

    @Override
    public void setTrueLabel(Label trueLabel) {
        this.trueLabel = trueLabel;
        left.setTrueLabel(trueLabel);
        right.setTrueLabel(trueLabel);
    }

    @Override
    public void setFalseLabel(Label falseLabel) {
        this.falseLabel = falseLabel;
        right.setFalseLabel(falseLabel);
    }
}
