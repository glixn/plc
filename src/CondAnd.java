public class CondAnd extends MetaLogicalCond {
    public CondAnd(MetaCond left, MetaCond right) {
        super(left,right);
        left.setTrueLabel(bridgeLabel);
    }

    @Override
    public void setTrueLabel(Label trueLabel) {
        this.trueLabel = trueLabel;
        right.setTrueLabel(trueLabel);
    }

    @Override
    public void setFalseLabel(Label falseLabel) {
        this.falseLabel = falseLabel;
        left.setFalseLabel(falseLabel);
        right.setFalseLabel(falseLabel);
    }
}
