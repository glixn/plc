public class Variable {
    private int num;

    public Variable(int num) {
        this.num = num;
    }

    @Override
    public String toString() {
        return "$t" + num;
    }
}
