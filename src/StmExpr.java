public class StmExpr extends MetaStm {
    private MetaExpr expr;

    public StmExpr(MetaExpr expr) {
        this.expr = expr;
    }

    @Override
    public String getCode() {
        return expr.preStatement();
    }
}
