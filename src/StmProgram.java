public class StmProgram extends MetaStm {
    private Program program;

    public StmProgram(Program program) {
        this.program = program;
    }

    @Override
    public String getCode() {
        return program.getProgram();
    }
}
