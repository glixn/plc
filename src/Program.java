import java.util.ArrayList;
import java.util.List;

public class Program {
    private List<MetaStm> stmList;

    public Program(MetaStm stm) {
        stmList = new ArrayList<MetaStm>();
        stmList.add(stm);
    }

    public void addStm(MetaStm stm) {
        stmList.add(stm);
    }

    public String getProgram() {
        StringBuilder program = new StringBuilder();
        for (MetaStm stm : stmList) {
            program.append(stm.getCode());
        }
        return program.toString();
    }
}
