public class Label {
    private int num;

    public Label(int num) {
        this.num = num;
    }

    public String gotoStm() {
        return "goto L" + num + ";\n";
    }

    public String labelStm() {
        return "label L" + num + ";\n";
    }

    @Override
    public String toString() {
        return "L" + num;
    }
}
