public class CondRelation extends MetaCond {
    private MetaExpr left;
    private MetaExpr right;
    private String operator;
    private boolean isTrue;

    public CondRelation(MetaExpr left, MetaExpr right, String operator, boolean isTrue) {
        this.left = left;
        this.right = right;
        this.operator = operator;
        this.isTrue = isTrue;
    }

    @Override
    public String condStatement() {
        return left.preStatement() + right.preStatement()
                + "if (" + left.value() + " " + operator + " " + right.value() + ") "
                + getJumpLabel().gotoStm()
                + getExitLabel().gotoStm();
    }

    private Label getJumpLabel() {
        return isTrue ? trueLabel : falseLabel;
    }

    private Label getExitLabel() {
        return isTrue ? falseLabel : trueLabel;
    }

    public void setTrueLabel(Label trueLabel) {
        this.trueLabel = trueLabel;
    }

    public void setFalseLabel(Label falseLabel) {
        this.falseLabel = falseLabel;
    }
}
