public class ExprSingle extends MetaExpr {
    private String value;

    public ExprSingle(String value) {
        this.value = value;
    }

    @Override
    public String preStatement() {
        return "";
    }

    @Override
    public String value() {
        return value;
    }
}
