public class StmIf extends MetaStm {
    private MetaCond condition;
    private MetaStm statement;

    public StmIf(MetaCond condition, MetaStm statement) {
        this.condition = condition;
        this.statement = statement;
        condition.setTrueLabel(Storage.newLabel());
        condition.setFalseLabel(Storage.newLabel());
    }

    @Override
    public String getCode() {
        return condition.condStatement()
                + condition.getTrueLabel().labelStm()
                + statement.getCode()
                + condition.getFalseLabel().labelStm();
    }
}
