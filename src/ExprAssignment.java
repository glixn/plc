public class ExprAssignment extends MetaExpr {
    private MetaExpr expr;
    private String value;

    public ExprAssignment(String varName, MetaExpr expr) {
        this.value = varName;
        this.expr = expr;
    }

    @Override
    public String preStatement() {
        String stm = expr.preStatement();
        stm += value + " = " + expr.value() + ";\n";
        return stm;
    }

    @Override
    public String value() {
        return value;
    }
}
