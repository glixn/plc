public class ExprMinus extends MetaExpr {
    private MetaExpr expr;
    private Variable value;

    public ExprMinus(MetaExpr expr) {
        this.expr = expr;
        this.value = Storage.newVariable();
    }

    @Override
    public String preStatement() {
        String stm = expr.preStatement();
        stm += value + " = -" + expr.value() + ";\n";
        return stm;
    }

    @Override
    public String value() {
        return value.toString();
    }
}
