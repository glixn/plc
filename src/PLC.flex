import java_cup.runtime.*;

%%
%cup

%%

// Reserved keywords
print       { return new Symbol(sym.PRINT); }
if          { return new Symbol(sym.IF); }
else        { return new Symbol(sym.ELSE); }
while       { return new Symbol(sym.WHILE); }
do          { return new Symbol(sym.DO); }
for         { return new Symbol(sym.FOR); }

// Classes of strings
[a-zA-Z][a-zA-Z0-9]* { return new Symbol(sym.IDENT, yytext() ); }
0|[1-9][0-9]*        { return new Symbol(sym.INT, yytext() ); }

/* Operators */
// Arithmetic
\*          { return new Symbol(sym.TIMES); }
\+          { return new Symbol(sym.PLUS); }
\-          { return new Symbol(sym.MINUS); }
\/          { return new Symbol(sym.DIV); }
\=          { return new Symbol(sym.ASSIGN); }

// Relational
"<"         { return new Symbol(sym.LT); }
"<="        { return new Symbol(sym.LTE); }
"=="        { return new Symbol(sym.EQ); }
"!="        { return new Symbol(sym.NEQ); }
">="        { return new Symbol(sym.GTE); }
">"         { return new Symbol(sym.GT); }
"!"         { return new Symbol(sym.NOT); }

// Logical
"&&"        { return new Symbol(sym.AND); }
"||"        { return new Symbol(sym.OR); }

// Control characters
\(			{ return new Symbol(sym.LP); }
\)			{ return new Symbol(sym.RP); }
\{			{ return new Symbol(sym.LB); }
\}			{ return new Symbol(sym.RB); }
\;			{ return new Symbol(sym.SC); }

// Fallback
[^]         { }