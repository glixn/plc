public abstract class MetaExpr {
    public abstract String preStatement();
    public abstract String value();
}
