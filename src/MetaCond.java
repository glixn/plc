public abstract class MetaCond {
    Label trueLabel;
    Label falseLabel;

    public abstract String condStatement();

    public Label getTrueLabel() {
        return trueLabel;
    }

    public Label getFalseLabel() {
        return falseLabel;
    }

    public abstract void setTrueLabel(Label trueLabel);

    public abstract void setFalseLabel(Label falseLabel);
}
