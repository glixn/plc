public class StmDoWhile extends MetaStm {
    private Label entryLabel;
    private MetaCond condition;
    private MetaStm statement;

    public StmDoWhile(MetaCond condition, MetaStm statement) {
        this.condition = condition;
        this.statement = statement;
        this.entryLabel = Storage.newLabel();
        condition.setTrueLabel(Storage.newLabel());
        condition.setFalseLabel(Storage.newLabel());
    }

    @Override
    public String getCode() {
        return entryLabel.labelStm()
                + statement.getCode()
                + condition.condStatement()
                + condition.getTrueLabel().labelStm()
                + entryLabel.gotoStm()
                + condition.getFalseLabel().labelStm();
    }
}
