public class StmWhile extends MetaStm {
    private Label entryLabel;
    private MetaCond condition;
    private MetaStm statement;

    public StmWhile(MetaCond condition, MetaStm statement) {
        this.condition = condition;
        this.statement = statement;
        this.entryLabel = Storage.newLabel();
        condition.setTrueLabel(Storage.newLabel());
        condition.setFalseLabel(Storage.newLabel());
    }

    @Override
    public String getCode() {
        return entryLabel.labelStm()
                + condition.condStatement()
                + condition.getTrueLabel().labelStm()
                + statement.getCode()
                + entryLabel.gotoStm()
                + condition.getFalseLabel().labelStm();
    }
}
