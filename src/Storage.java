public class Storage {
    private static int nextVar = 0;
    private static int nextLabel = 0;

    public static Variable newVariable() {
        Variable var = new Variable(nextVar);
        nextVar++;
        return var;
    }

    public static Label newLabel() {
        Label label = new Label(nextLabel);
        nextLabel++;
        return label;
    }
}
