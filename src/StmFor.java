public class StmFor extends MetaStm {
    private Label entryLabel;
    private MetaExpr initial;
    private MetaCond condition;
    private MetaExpr iterate;
    private MetaStm statement;

    public StmFor(MetaExpr initial, MetaCond condition, MetaExpr iterate, MetaStm statement) {
        this.initial = initial;
        this.condition = condition;
        this.iterate = iterate;
        this.statement = statement;
        this.entryLabel = Storage.newLabel();
        condition.setTrueLabel(Storage.newLabel());
        condition.setFalseLabel(Storage.newLabel());
    }

    @Override
    public String getCode() {
        return initial.preStatement()
                + entryLabel.labelStm()
                + condition.condStatement()
                + condition.getTrueLabel().labelStm()
                + statement.getCode()
                + iterate.preStatement()
                + entryLabel.gotoStm()
                + condition.getFalseLabel().labelStm();
    }
}
