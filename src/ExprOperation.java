public class ExprOperation extends MetaExpr {
    private MetaExpr left;
    private MetaExpr right;
    private Variable label;
    private char sign;

    public ExprOperation(MetaExpr left, MetaExpr right, char sign) {
        this.left = left;
        this.right = right;
        this.sign = sign;
        this.label = Storage.newVariable();
    }

    @Override
    public String preStatement() {
        String stm = left.preStatement() + right.preStatement();
        stm += label.toString() + " = " + left.value() + " " + sign + " " + right.value() + ";\n";
        return stm;
    }

    @Override
    public String value() {
        return label.toString();
    }
}
