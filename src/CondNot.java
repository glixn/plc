public class CondNot extends MetaCond {
    private MetaCond condition;

    public CondNot(MetaCond condition) {
        this.condition = condition;
    }

    @Override
    public String condStatement() {
        return condition.condStatement();
    }

    @Override
    public void setTrueLabel(Label trueLabel) {
        this.trueLabel = trueLabel;
        condition.setFalseLabel(trueLabel);
    }

    @Override
    public void setFalseLabel(Label falseLabel) {
        this.falseLabel = falseLabel;
        condition.setTrueLabel(falseLabel);
    }
}
